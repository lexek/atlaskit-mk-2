# @atlaskit/pubsub

## 2.0.9
- [patch] Clean Changelogs - remove duplicates and empty entries [e7756cd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e7756cd)
- [none] Updated dependencies [e7756cd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e7756cd)
  - @atlaskit/field-text@6.0.4
  - @atlaskit/button@8.1.2
  - @atlaskit/theme@4.0.4
  - @atlaskit/lozenge@5.0.4

## 2.0.8
- [none] Updated dependencies [9d20f54](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9d20f54)
  - @atlaskit/docs@4.1.0
  - @atlaskit/util-service-support@2.0.10
  - @atlaskit/theme@4.0.2
  - @atlaskit/lozenge@5.0.2
  - @atlaskit/field-text@6.0.2
  - @atlaskit/button@8.1.0

## 2.0.7
- [patch] FS-797 Allow setting url for pubsub example and fix url-search-params import style [1c85e67](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1c85e67)
- [none] Updated dependencies [1c85e67](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1c85e67)
  - @atlaskit/util-service-support@2.0.9

## 2.0.6
- [patch] Updated dependencies [1e80619](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e80619)
  - @atlaskit/field-text@6.0.0
  - @atlaskit/button@8.0.0
  - @atlaskit/theme@4.0.0
  - @atlaskit/docs@4.0.0
  - @atlaskit/util-service-support@2.0.8

## 2.0.5
- [patch]  [f87724e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f87724e)

## 2.0.4
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/field-text@5.0.3
  - @atlaskit/button@7.2.5
  - @atlaskit/theme@3.2.2
  - @atlaskit/docs@3.0.4
  - @atlaskit/util-service-support@2.0.7

## 2.0.2
- [patch] FS-1948 Stop pubsub client from flooding console with 403 [bb3d588](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bb3d588)

## 2.0.0
- [major] Update to React 16.3. [8a96fc8](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8a96fc8)

## 1.1.0
- [minor] FS-1874 Move @atlassian/pubsub to @atlaskit/pubsub [92af7f7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/92af7f7)
