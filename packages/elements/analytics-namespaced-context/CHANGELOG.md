# @atlaskit/analytics-namespaced-context

## 1.0.1
- [patch] wrapper for analytics-next AnalyticsContext to add a namespace [91e5997](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/91e5997)
