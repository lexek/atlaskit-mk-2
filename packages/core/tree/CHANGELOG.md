# @atlaskit/tree

## 0.1.1
- [patch] Bump version of spinner [1adf8d1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1adf8d1)

## 0.1.0
- [minor] Developer preview version of @atlaskit/tree component [79b10a4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/79b10a4)
