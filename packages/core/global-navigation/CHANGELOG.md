# @atlaskit/global-navigation

## 2.0.0
- [major] Extract standalone Drawer component. Remove drawer state from navigation state manager navigation-next. Stop exporting Drawer component in global-navigation [d11307b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d11307b)
- [major] Updated dependencies [d11307b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d11307b)
  - @atlaskit/navigation-next@1.0.0
  - @atlaskit/drawer@0.1.0

## 1.0.0
- [major] Update props api for global-navigation. Change the way ResizeControl works in navigation-next [1516d79](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1516d79)
- [major] Updated dependencies [1516d79](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1516d79)
  - @atlaskit/navigation-next@0.3.4

## 0.1.3
- [patch] Clean Changelogs - remove duplicates and empty entries [e7756cd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e7756cd)
- [patch] Updated dependencies [e7756cd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e7756cd)
  - @atlaskit/onboarding@4.1.2
  - @atlaskit/theme@4.0.4
  - @atlaskit/navigation-next@0.3.3
  - @atlaskit/icon@12.1.2
  - @atlaskit/dropdown-menu@5.0.4

## 0.1.2
- [patch] Updated dependencies [7200aa4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7200aa4)
  - @atlaskit/navigation-next@0.3.2

## 0.1.1
- [patch] Update changelogs to remove duplicate [cc58e17](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/cc58e17)
- [patch] Updated dependencies [cc58e17](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/cc58e17)
  - @atlaskit/theme@4.0.3
  - @atlaskit/onboarding@4.1.1
  - @atlaskit/navigation-next@0.3.1
  - @atlaskit/icon@12.1.1
  - @atlaskit/dropdown-menu@5.0.3
  - @atlaskit/blanket@6.0.3
  - @atlaskit/avatar@11.1.1
  - @atlaskit/docs@4.1.1

## 0.1.0
- [none] Updated dependencies [9d20f54](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9d20f54)
  - @atlaskit/onboarding@4.1.0
  - @atlaskit/navigation-next@0.3.0
  - @atlaskit/dropdown-menu@5.0.2
  - @atlaskit/avatar@11.1.0
  - @atlaskit/icon@12.1.0
  - @atlaskit/docs@4.1.0
  - @atlaskit/theme@4.0.2
  - @atlaskit/blanket@6.0.2

## 0.0.10
- [patch] Updated dependencies [ba0ba79](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ba0ba79)
  - @atlaskit/navigation-next@0.2.2

## 0.0.9
- [patch] Update readme's [223cd67](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/223cd67)
- [patch] Updated dependencies [223cd67](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/223cd67)
  - @atlaskit/navigation-next@0.2.1
  - @atlaskit/onboarding@4.0.1
  - @atlaskit/icon@12.0.1
  - @atlaskit/theme@4.0.1
  - @atlaskit/blanket@6.0.1
  - @atlaskit/docs@4.0.1
  - @atlaskit/dropdown-menu@5.0.1
  - @atlaskit/avatar@11.0.1

## 0.0.8
- [patch] Updated dependencies [41f5218](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/41f5218)
  - @atlaskit/navigation-next@0.2.0

## 0.0.7
- [patch] Updated dependencies [1e80619](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e80619)
  - @atlaskit/navigation-next@0.1.3
  - @atlaskit/onboarding@4.0.0
  - @atlaskit/icon@12.0.0
  - @atlaskit/theme@4.0.0
  - @atlaskit/blanket@6.0.0
  - @atlaskit/docs@4.0.0
  - @atlaskit/dropdown-menu@5.0.0
  - @atlaskit/avatar@11.0.0

## 0.0.6
- [patch] Updated dependencies [615e77c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/615e77c)
  - @atlaskit/navigation-next@0.1.2

## 0.0.5
- [patch] Updated dependencies [3882051](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3882051)
  - @atlaskit/navigation-next@0.1.1

## 0.0.4
- [patch] Bumping dep on navigation-next [548787e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/548787e)

## 0.0.3
- [patch] Rename props to be in sync with navigation-next package [1fde1da](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1fde1da)

## 0.0.2
- [patch] Add global-navigation package [41a4d1c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/41a4d1c)
- [patch] Updated dependencies [7c99742](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7c99742)
  - @atlaskit/navigation-next@0.0.7
