# Needs Docs

The docs for this package have been migrated from the old Atlaskit repo as-is,
and should be expanded to properly explain how theming works (including how this
package is intended to replace util-shared-styles).
