import { Fragment, Node, Slice, Schema } from 'prosemirror-model';
import { hasParentNodeOfType } from 'prosemirror-utils';
import { EditorState } from 'prosemirror-state';

export type FlatMapCallback = (
  node: Node,
  index: number,
  fragment: Fragment,
) => Node | Node[];

export function flatmap(
  fragment: Fragment,
  callback: FlatMapCallback,
): Fragment {
  const fragmentContent = [] as Node[];
  for (let i = 0; i < fragment.childCount; i++) {
    const child = callback(fragment.child(i), i, fragment);
    if (Array.isArray(child)) {
      fragmentContent.push(...child);
    } else {
      fragmentContent.push(child);
    }
  }
  return Fragment.fromArray(fragmentContent);
}

export function unwrapContentFromLayout(
  maybeLayoutSection: Node,
): Node | Node[] {
  const { schema } = maybeLayoutSection.type;
  if (maybeLayoutSection.type === schema.nodes.layoutSection) {
    const content = [] as Node[];
    maybeLayoutSection.content.forEach(maybeLayoutColumn => {
      // Content in some cases isn't wrapped with a layoutColumn, so leave the contents as is
      if (maybeLayoutColumn.type === schema.nodes.layoutColumn) {
        maybeLayoutColumn.forEach(node => {
          content.push(node);
        });
      } else {
        content.push(maybeLayoutColumn);
      }
    });
    return content;
  }
  return maybeLayoutSection;
}

export function removeLayoutFromFirstChild(node: Node, i: number) {
  return i === 0 ? unwrapContentFromLayout(node) : node;
}

export function removeLayoutFromLastChild(
  node: Node,
  i: number,
  fragment: Fragment,
) {
  return i === fragment.childCount - 1 ? unwrapContentFromLayout(node) : node;
}

export function removeLayoutFromAllChildren(node: Node) {
  return unwrapContentFromLayout(node);
}

/**
 * When we have a slice that cuts across a layoutSection/layoutColumn
 * we can end up with unexpected behaviour on paste/drop where a user
 * is able to add columns to a layoutSection. By 'lifting' any content
 * inside an 'open' layoutSection/layoutColumn to the top level, we
 * can ensure prevent this.
 *
 * We only care about slices with non-zero openStart / openEnd's here
 * as we're totally fine for people to copy/paste a full layoutSection
 */
export function transformSliceToRemoveOpenLayoutNodes(
  slice: Slice,
  schema: Schema,
) {
  // Case 1: A slice entirely within a single layoutSection
  if (slice.openStart && slice.openEnd && slice.content.childCount === 1) {
    const maybeLayoutSection = slice.content.firstChild!;
    if (maybeLayoutSection.type === schema.nodes.layoutSection) {
      return new Slice(
        flatmap(slice.content, removeLayoutFromAllChildren),
        // '-2' here because we've removed the layoutSection/layoutColumn; reducing the open depth.
        slice.openStart - 2,
        slice.openEnd - 2,
      );
    }
  }

  // Case 2: A slice starting inside a layoutSection and finishing outside
  if (
    slice.openStart &&
    slice.content.firstChild!.type === schema.nodes.layoutSection
  ) {
    slice = new Slice(
      flatmap(slice.content, removeLayoutFromFirstChild),
      slice.openStart - 2,
      slice.openEnd,
    );
  }

  // Case 3: A slice starting outside a layoutSection and finishing inside
  if (
    slice.openEnd &&
    slice.content.lastChild!.type === schema.nodes.layoutSection
  ) {
    slice = new Slice(
      flatmap(slice.content, removeLayoutFromLastChild),
      slice.openStart,
      slice.openEnd - 2,
    );
  }

  // Case 2 & 3 also handles a slice starting in one layoutSection & finishing in a different layoutSection

  return slice;
}

export function removeLayoutsIfSelectionIsInLayout(
  slice: Slice,
  state: EditorState,
) {
  // If pasting into a layout, remove any layouts from the slice
  const isSelectionInLayout = hasParentNodeOfType(
    state.schema.nodes.layoutColumn,
  )(state.selection);
  if (isSelectionInLayout) {
    const sliceWithNoLayouts = new Slice(
      flatmap(slice.content, removeLayoutFromAllChildren),
      // transformPasted ensure the start/end node of the slice won't be a layout, so these won't change
      slice.openStart,
      slice.openEnd,
    );
    return sliceWithNoLayouts;
  }
  return slice;
}
