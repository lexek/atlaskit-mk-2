# @atlaskit/conversation

## 8.2.5
- [patch] Updated dependencies [4faccc0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4faccc0)
  - @atlaskit/renderer@18.2.5
  - @atlaskit/editor-common@11.3.0
  - @atlaskit/editor-core@73.8.6

## 8.2.4
- [patch] Updated dependencies [8efe0af](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8efe0af)
  - @atlaskit/comment@5.0.0

## 8.2.3
- [patch] Updated dependencies [4e4825e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4e4825e)
  - @atlaskit/editor-core@73.8.0
  - @atlaskit/editor-common@11.2.6

## 8.2.2
- [patch] Clean Changelogs - remove duplicates and empty entries [e7756cd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e7756cd)
- [none] Updated dependencies [e7756cd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e7756cd)
  - @atlaskit/util-data-test@9.1.13
  - @atlaskit/reactions@12.1.3
  - @atlaskit/renderer@18.1.2
  - @atlaskit/editor-core@73.7.5
  - @atlaskit/comment@4.1.2
  - @atlaskit/editor-common@11.2.1
  - @atlaskit/single-select@5.1.2

## 8.2.1
- [patch] Update changelogs to remove duplicate [cc58e17](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/cc58e17)
- [none] Updated dependencies [cc58e17](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/cc58e17)
  - @atlaskit/util-data-test@9.1.12
  - @atlaskit/renderer@18.1.1
  - @atlaskit/editor-core@73.7.1
  - @atlaskit/comment@4.1.1
  - @atlaskit/editor-common@11.1.2
  - @atlaskit/single-select@5.1.1
  - @atlaskit/avatar@11.1.1
  - @atlaskit/docs@4.1.1

## 8.2.0
- [none] Updated dependencies [7217164](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7217164)
  - @atlaskit/editor-core@73.5.0
  - @atlaskit/renderer@18.1.0
  - @atlaskit/util-data-test@9.1.11
  - @atlaskit/reactions@12.1.0
  - @atlaskit/editor-common@11.1.0
  - @atlaskit/comment@4.1.0

## 8.1.1
- [patch] Updated dependencies [b7a4fd5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b7a4fd5)
  - @atlaskit/editor-core@73.4.6

## 8.1.0
- [minor] Updated dependencies [cad95fa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/cad95fa)
  - @atlaskit/editor-core@73.2.0

## 8.0.0
- [major] makes styled-components a peer dependency and upgrades version range from 1.4.6 - 3 to ^3.2.6 [1e80619](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e80619)
- [patch] Updated dependencies [1e80619](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e80619)
  - @atlaskit/util-data-test@9.1.10
  - @atlaskit/reactions@12.0.12
  - @atlaskit/renderer@18.0.0
  - @atlaskit/editor-core@73.0.0
  - @atlaskit/comment@4.0.0
  - @atlaskit/editor-common@11.0.0
  - @atlaskit/single-select@5.0.0
  - @atlaskit/docs@4.0.0
  - @atlaskit/avatar@11.0.0

## 7.6.3
- [patch] Updated dependencies [1c87e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1c87e5a)
  - @atlaskit/util-data-test@9.1.9
  - @atlaskit/reactions@12.0.11
  - @atlaskit/renderer@17.0.9
  - @atlaskit/editor-core@72.2.2
  - @atlaskit/comment@3.1.9
  - @atlaskit/editor-common@10.1.9

## 7.6.2
- [patch] Comments should re-render when the user is changed [507ff28](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/507ff28)

## 7.6.1
- [patch] Fixes a bug with temp comment where createdBy wasn't set correctly [a761abc](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a761abc)

## 7.6.0
- [minor] Replace <div> with Fragment so CSS rules apply to adjacent conversations [96dc1bf](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/96dc1bf)

## 7.5.6
- [patch] Updated dependencies [179332e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/179332e)
  - @atlaskit/renderer@17.0.7

## 7.5.5
- [patch] Updated dependencies [41eb1c1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/41eb1c1)
  - @atlaskit/editor-common@10.1.3
  - @atlaskit/renderer@17.0.6

## 7.5.4
- [patch] Updated dependencies [758b342](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/758b342)
  - @atlaskit/renderer@17.0.2
  - @atlaskit/editor-core@72.0.7

## 7.5.3
- [none] Updated dependencies [febc44d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/febc44d)
  - @atlaskit/editor-core@72.0.0
  - @atlaskit/renderer@17.0.0
  - @atlaskit/util-data-test@9.1.4
  - @atlaskit/reactions@12.0.7
  - @atlaskit/editor-common@10.0.0
  - @atlaskit/comment@3.1.8

## 7.5.2
- [patch] Updated dependencies [1764815](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1764815)
  - @atlaskit/reactions@12.0.6

## 7.5.1
- [none] Updated dependencies [8fd4dd1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8fd4dd1)
  - @atlaskit/renderer@16.2.6
  - @atlaskit/util-data-test@9.1.3
  - @atlaskit/reactions@12.0.5
  - @atlaskit/editor-core@71.4.0
  - @atlaskit/comment@3.1.6
  - @atlaskit/editor-common@9.3.9

## 7.5.0
- [minor] Add option to allow feedback and help buttons [ca8bdc3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ca8bdc3)

## 7.4.0
- [minor] Add ability to disable scroll-to behavior and permalinks [94792ca](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/94792ca)

## 7.2.0
- [minor] Support for main conversations. Fixes margin-right on editor component. Added scroll-behavior. [58a90ba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/58a90ba)

## 7.1.14
- [patch] Avatar should display tooltip on hover and be clickable if profile url is provided [272893c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/272893c)

## 7.1.12
- [patch] Close empty editor on save [e5a9f36](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e5a9f36)

## 7.1.8
- [patch] Fix permalink check [8d715f1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8d715f1)

## 7.1.3
- [patch] Close Editor when comment is saved [ae181bf](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ae181bf)

## 7.1.1
- [patch] Adding placholder prop for optionally setting the editor placholder text [9f1696a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9f1696a)

## 7.1.0
- [minor] Adding permalink support [c79d549](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c79d549)

## 7.0.1
- [patch] Don't allow empty comments [cd9069c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/cd9069c)

## 7.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 6.3.6
- [patch] Fixes rendering of the editor in IE11 [df91076](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/df91076)

## 6.3.3
- [patch] Fix for styled-components types to support v1.4.x [75a2375](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/75a2375)

## 6.3.0
- [minor] Adding support for reactions [1b74cff](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1b74cff)

## 6.2.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 6.1.1
- [patch] Make textFormatting and hyperlink plugins default [689aa8d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/689aa8d)

## 6.1.0
- [minor] Adding renderEditor prop [e2485f7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e2485f7)

## 6.0.0
- [major] Replacing internal store with Redux [703bb99](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/703bb99)

## 5.2.5
- [patch] Adding documentations and fixed prop-types [4430cbb](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4430cbb)

## 5.2.3
- [patch] updated the repository url to https://bitbucket.org/atlassian/atlaskit-mk-2 [1e57e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e57e5a)

## 5.2.2
- [patch] Enabling hyperlinks in the conversation editor [a151fd3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a151fd3)

## 5.2.0
- [minor] Add subscribe/unsubscribe methods to resource provider [5c1e2bf](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5c1e2bf)

## 5.1.1
- [patch] bump editor-common to 6.1.2 [bb7802e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bb7802e)

## 5.1.0
- [minor] Add error state and cancel/retry [0c1bb6e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0c1bb6e)

## 5.0.3
- [patch] bump mention to 9.1.1 to fix mention autocomplete bug [c7708c6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c7708c6)

## 5.0.2
- [patch] Avoid unnecessary re-render of comments [402cc7c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/402cc7c)

## 5.0.0
- [major] Added some tests for reducer/store and renamed actions [ba629ef](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ba629ef)

## 4.9.0
- [minor] Adding optimistic updates [45922f3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/45922f3)

## 4.8.1
- [patch] Fixes the avatar next to the editor and hides reply functionality if user isn't set [0123c1d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0123c1d)

## 4.8.0
- [minor] Add optional onUserClick handler [40f2e90](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/40f2e90)

## 4.7.0
- [minor] Add emoji/mention support [846baed](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/846baed)

## 4.6.0
- [minor] Fix delete comment to accept 204 [83b5f70](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/83b5f70)

## 4.5.0
- [minor] Add comment delete functionality [e26446a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e26446a)

## 4.4.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 4.3.0
- [minor] feature/ED-3471 Add comment edit functionality [c474f67](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c474f67)

## 4.0.0
- [major] New API [41633b9](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/41633b9)

## 3.0.10
- [patch] Fix dependencies in CI [670e930](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/670e930)

## 3.0.9
- [patch] dummy changeset to initiate release [ba17f5b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ba17f5b)

## 3.0.0
- [major] Changing the API to match the service [b308326](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b308326)

## 2.0.0
- [major] Conversation Component [bc1a3a4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bc1a3a4)
