export interface ContextIdentifierProvider {
  containerId: string;
  objectId: string;
}
