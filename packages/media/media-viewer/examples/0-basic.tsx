import * as React from 'react';

export default () => (
  <div>
    <p>
      Currently we don't support examples for this package since it has a
      implicit dependency on the old MediaViewer (@atlassian/mediaviewer) that
      lives in the private Atlassian registry.
    </p>

    <p>
      In order to see a demo of it working, please have a look into the{' '}
      <a href="https://media-playground.internal.app.dev.atlassian.io/chat">
        MediaPlayground
      </a>
    </p>
  </div>
);
