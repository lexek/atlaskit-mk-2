import { md } from '@atlaskit/docs';

export default md`
  # MediaUI

  > Includes common components and utilities used by other media packages
`;
