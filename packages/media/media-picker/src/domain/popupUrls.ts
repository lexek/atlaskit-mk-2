export interface PopupUrls {
  apiUrl: string;
  redirectUrl: string;
}
